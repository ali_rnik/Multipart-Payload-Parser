#include <iostream>
#include <string>
#include <fstream>
#include <sstream>

#define START_OF_NAME 6
#define START_OF_FILENAME 10



int main(int argc, char **argv)
{
  static long long payload_length;
  static std::string payload_file;
  static std::string payload_seperator;
  static std::string payload_end;
  static std::ofstream outstream;
  static std::ifstream instream;
  static std::string buf, name, filename, body;
  static std::stringstream ss;
  static int start_p, end_p;

  payload_end.clear();

  payload_length = atoll(argv[1]);
  payload_file = argv[2];
  payload_seperator = argv[3];
  payload_end += payload_seperator + "--";

  instream.clear();
  outstream.clear();

  instream.open(payload_file);

  while (1) {
    ss.clear(), buf.clear(), filename.clear(), name.clear(), body.clear();
    instream >> buf;

    if (buf == payload_end)
      break;

    if (buf == payload_seperator) {
      instream.ignore();
      std::getline(instream, buf);

      ss.str(buf);
      ss >> buf, ss >> buf;

      ss >> buf;
      name = buf.substr(START_OF_NAME);
      name.pop_back();

      buf.clear();
      ss >> buf;
      if (!buf.empty()) {
        filename = buf.substr(START_OF_FILENAME);
        filename.pop_back();

        std::getline(instream, buf);
      }

      body.clear();
      start_p = instream.tellg();
      while (1) {
        instream >> buf;
        if (buf == payload_seperator) {
          for (int i = 0; i < payload_seperator.size(); i++)
            instream.unget();
          break;
        }
        else if (buf == payload_end) {
          for (int i = 0; i < payload_end.size(); i++)
            instream.unget();
          break;
        }
      }
      end_p = instream.tellg();
      body.resize(end_p - start_p);
      instream.seekg(start_p);
      instream.read(&body[0], end_p - start_p);
      instream.seekg(end_p);
    }

    if (!filename.empty()) {
      outstream.open(filename);
      outstream.write(body.data(), body.size());
    }
    else {
      outstream.open(name);
      outstream.write(body.data(), body.size());
    }

    outstream.close();
  }

  instream.close();
}

